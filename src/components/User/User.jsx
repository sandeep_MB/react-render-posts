import React, { Component } from "react";
import "./User.css";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: [],
      error: false,
    };
  }

  componentDidMount() {
    const path = window.location.pathname;
    const index = path.split("/");
    const id = index[index.length - 1];

    fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        this.setState({ user: data, error: true });
        console.log(this.state.user);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const { user } = this.state;
    return (
      <div>
        {this.state.error ? (
          <div className="user-info">
            <div className="user-name">
              <h3>
                <i class="fas fa-user"></i>
                {user.name} @ {user.username}
              </h3>
            </div>

            <div className="lower-box">
              <div className="more-details">
                <h4>Address:-</h4>
                <ul>
                  <li>Street:- {user.address.street}</li>
                  <li>Suite:-{user.address.suite}</li>
                  <li>City:-{user.address.city}</li>
                  <li>Zipcode:-{user.address.zipcode}</li>
                </ul>
                <h4>Company name:</h4>
                <p>{user.company.name}</p>
                <p>{user.company.catchPhrase}</p>
              </div>
              <div className="basic-details">
                <h4>Phone Number</h4>
                <p>{user.phone}</p>
                <h4>Email:-</h4>
                <p>{user.email}</p>
                <h4>Website</h4>
                <p>{user.website}</p>
              </div>
            </div>
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default User;
