import React, { Component } from "react";
import "./Post.css";
class Post extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {},
      user: {},
      error: false,
    };
  }
  getUserData(id) {
    fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ user: data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getPostData(postId) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState({ post: data });
        this.getUserData(data.userId);
        this.getCommentData(data.id);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getCommentData(postId) {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`)
      .then((res) => res.json())
      .then((data) => {
        this.setState({ comments: data, error: true });
      })
      .catch((err) => {
        console.error(err);
      });
  }
  componentDidMount() {
    const path = window.location.pathname;
    const index = path.split("/");
    const id = index[index.length - 1];

    this.getPostData(id);
  }

  render() {
    const { post, user, comment } = this.state;
    return (
      <div>
        {this.state.error ? (
          <div>
            <div>
              <div>
                <div>
                  <div className="user-name">
                    <i class="fa fa-user"></i>
                    {user.name}
                    <div>@{user.username}</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="title-body-container">
              <h2>{post.title}</h2>
              <div>{post.body}</div>
            </div>
            <div className="comment-container">
              {this.state.comments.map((com, index) => {
                if (com.postId === post.id) {
                  return (
                    <div className="comment">
                      <h4>Comment:{index + 1}</h4>
                      <p className="name">
                        <span>Name:</span> {com.name}
                      </p>
                      <p className="email">
                        <span>Email:</span> {com.email}
                      </p>
                      <p className>
                        <span>Comment:</span> {com.body}
                      </p>
                    </div>
                  );
                }
              })}
            </div>
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default Post;
