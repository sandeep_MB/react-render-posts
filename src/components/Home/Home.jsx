import React, { Component } from "react";
import "./Home.css";
import { Link } from "react-router-dom";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      posts: [],
      comments: [],
      toggleComm: Array(101).fill(false),
    };
  }

  getUserData() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        this.setState((this.state.users = data));
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getPostData() {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        // let newData = data.filter((data) => id === data.userId);
        this.setState({ posts: data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  getCommentData() {
    fetch("https://jsonplaceholder.typicode.com/comments")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ comments: data });
      })
      .catch((err) => {
        console.error(err);
      });
  }
  componentDidMount() {
    this.getPostData();
    this.getUserData();
    this.getCommentData();
  }

  toggleCommArr(id) {
    const tempArr = this.state.toggleComm;
    tempArr[id] = !tempArr[id];
    this.setState({ toggleComm: tempArr });
  }

  render() {
    return (
      <div className="post-data">
        {this.state.posts.map((post) => (
          <div className="post" key={post.id}>
            <div className="heading">
              {this.state.users.map((user) => {
                if (post.userId === user.id) {
                  return (
                    <div className="user-name">
                      <img
                        className="user-image"
                        src="https://www.incimages.com/uploaded_files/image/1920x1080/getty_146107054_2000133020009280105_347552.jpg"
                      ></img>
                      <h2>{user.name}</h2>{" "}
                      <Link to={`/user/${user.id}`}>
                        <span>@{user.username}</span>
                      </Link>
                    </div>
                  );
                }
              })}
            </div>
            <div className="title-body-container">
              <div className="title">{post.title}</div>
              <div className="body">{post.body}</div>
              <p>
                <Link to={`/post/${post.id}`}>See post</Link>
              </p>
            </div>
            <div>
              <button
                className="btn-comment"
                onClick={() => {
                  this.toggleCommArr(post.id);
                }}
              >
                Get Comments
              </button>
              {this.state.toggleComm[post.id]
                ? this.state.comments.map((com) => {
                    if (com.postId === post.id) {
                      return (
                        <div className="comment">
                          <p className="name">
                            <span>Name:</span> {com.name}
                          </p>
                          <p className="email">
                            <span>Email:</span> {com.email}
                          </p>
                          <p className>
                            <span>Comment:</span> {com.body}
                          </p>
                        </div>
                      );
                    }
                  })
                : undefined}
            </div>
          </div>
        ))}
        <div className="comments" onClick={() => this.getCommentData()}>
          {this.state.commentData}
        </div>
      </div>
      // </div>
    );
  }
}
export default Home;
