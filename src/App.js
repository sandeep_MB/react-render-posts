import React, { Component } from "react";
import { Routes, Route, Link } from "react-router-dom";
import Home from "./components/Home/Home";
import User from "./components/User/User";
import Post from "./components/Post/Post";

class App extends React.Component {
  render() {
    return (
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/user/:id" element={<User />} />
        <Route path="/post/:id" element={<Post />} />
      </Routes>
      // <div>
      //   <User />
      // </div>
    );
  }
}

export default App;
